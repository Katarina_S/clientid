create DATABASE PERSON;
use PERSON;
create table PERSON
(
id BIGINT not null AUTO_INCREMENT,
name varchar (255),
second_name varchar (255),
PRIMARY KEY (id)
);
commit;
alter table PERSON
  add constraint PERSON primary key (ID);

insert into PERSON ( NAME, SECOND_NAME) values ('Sade', 'Krame');
insert into PERSON ( NAME, SECOND_NAME) values ('Kate', 'Krame');
insert into PERSON (NAME, SECOND_NAME) values ('Tom', 'Bance');
insert into PERSON (NAME, SECOND_NAME) values ('Busechkin', 'Lovely');

