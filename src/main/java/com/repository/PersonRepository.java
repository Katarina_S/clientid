package com.repository;

import com.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    Optional<Person> findById(Long id);
    Person findPersonById (Long id);
    //void save(Person person, Long id);
    List<Person> findPersonByName(String name);
    List<Person> getAllByNameNotNull();
    //Person findByIdIsNotNull(Long id);
}
