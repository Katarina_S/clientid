package com.service;

import com.dto.PersonDto;
import com.entity.Person;
import com.repository.PersonRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Primary
@Transactional
@Service
public class DefaultPersonService implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<PersonDto> getAllPersons() {
        List<Person> personList = personRepository.findAll();
        System.out.println(personList);
        return personList.stream()
                   .map(this::convertToDto)
                   .collect(Collectors.toList());
    }

    @Override
    public PersonDto getPersonById(Long id) {
        return convertToDto(personRepository.findPersonById(id));
    }

    @Override
    public PersonDto addPerson(PersonDto personDto) {
        Person person = new Person();
        person.setName(person.getName());
        person.setSecondName(personDto.getSecondName());
        return convertToDto(personRepository.save(person));
    }

    @Override
    public PersonDto updatePerson(PersonDto personDto) {
        Person person = personRepository.findPersonById(personDto.getId());

        if(person == null) {
            throw new EntityNotFoundException("Ooops, this person we overlooked or is a secret agent");
        }

        person.setName(person.getName());
        person.setSecondName(personDto.getSecondName());
        return convertToDto(personRepository.save(person));
    }

    @Override
    public void deletePersonById(Long id) {
        if(id == null){
            throw new EntityNotFoundException("This can not be because this can not be never!!!!  P.S. We lost Person Id (((");
        }

        Person person = personRepository.findPersonById(id);

        if(person == null){
            throw new EntityNotFoundException("Company = null");
        }

        personRepository.deleteById(id);
    }

    private PersonDto convertToDto (Person person) {
        return  modelMapper.map(person, PersonDto.class);
    }
}
