package com.service;

import com.dto.PersonDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PersonService {
    List<PersonDto> getAllPersons();
    PersonDto getPersonById(Long id);
    PersonDto addPerson(PersonDto personDto);
    PersonDto updatePerson (PersonDto personDto);
    void deletePersonById (Long id);
}
