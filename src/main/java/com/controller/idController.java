package com.controller;

import com.dto.PersonDto;
import com.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class idController {

    @Autowired
    private PersonService personService;

    @PostMapping("/person")
    public PersonDto addPerson (@RequestBody PersonDto personDto) {
        return personService.addPerson(personDto);
    }

    @PutMapping("/person/{personId}")
    public void updatePerson (@PathVariable Long personId, @RequestBody PersonDto personDto) {
        personDto.setId(personId);
        personService.updatePerson(personDto);
    }

    @GetMapping("/persons")
    public List<PersonDto> getAllPersons () {
        return personService.getAllPersons();
    }

    @GetMapping("/person/{personId}")
    public PersonDto getPersonById (@PathVariable Long personId) {
        return personService.getPersonById(personId);
    }

    @DeleteMapping("/person/{personId}")
    public void deletePerson (@PathVariable Long personId) {
        personService.deletePersonById(personId);
    }

    @RequestMapping("/")
    public String index(){
        return "there is nothing to see here";
    }
}
